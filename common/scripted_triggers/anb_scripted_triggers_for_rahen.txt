#-------------Castes Fluidity-------------#
caste_fluidity_is_fluid = {
	check_variable = { which = casteFluidityVar value = 66 }
}

caste_fluidity_is_neutral = {
	NOT = { check_variable = { which = casteFluidityVar value = 66 } }
	check_variable = { which = casteFluidityVar value = 33 }
}

caste_fluidity_is_rigid = {
	NOT = { check_variable = { which = casteFluidityVar value = 33 } }
}